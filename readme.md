Имеется код парсера, схема команды и несколько тестов от разработчика.
Текущий парсер должен уметь парсить команды
  CMD
  CMD NEXT
и должен возвращать false во всех остальных случаях

Поступило задание добавить поддержку нового ключа в команде
  CMD
  CMD NEXT
  CMD PREV

При этом команды
  CMD NEXT PREV
  CMD PREV NEXT
должны возвращать false

Код можно выполнить кнопкой Run в левом верхнем углу или ctrl+enter.
Выполненное задание можно сохранить нажав ctrl+s (изменится урл - его и нужно отправить как результат)
Либо прислать целиком JS код на почту