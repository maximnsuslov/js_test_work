var Parser = function() {
};

Parser.prototype = {
  parse: function(str, params) {
    var result = {};
    var rest;
    try {
      rest = str;
      for (var i = 0; i < params.length; i++) {
        rest = this.applyParam(params[i], rest, result);
      }
    } catch (ex) {
      return false;
    }
    if (typeof rest === 'string') {
      rest = rest.trim();
      if (rest.length > 0) {
        throw new Error('Invalid command');
      }
    }
    return result;
  },

  applyParam: function(p, str, result) {
    switch (p.kind) {
      case "match":
        str = this.applyMatch(p, str, result);
        break;

      case 'mandatory':
        str = this.applyMandatory(p, str, result);
        break;

      case 'optional':
        str = this.applyOptional(p, str, result);
        break;

      case 'separator':
        str = this.applySeparator(p, str, result);
        break;

      default:
        throw new Error('Unknown param kind');
    }
    return str;
  },

  applyMatch: function(p, str, result) {
    if (str.length < 1) {
      throw new Error('Nothing to match');
    }
    var re = new RegExp(p.match, p.flags || 'g');
    var matched = re.exec(str);
    if (matched && matched.index === 0) {
      str = str.substr(matched[0].length);
      if (p.fieldName) {
        result[p.fieldName] = matched[1];
      }
    } else {
      throw new Error('No match');
    }
    return str;
  },

  applyMandatory: function(p, str, result) {
    var self = this;
    p.matches.forEach(function(m) {
      str = self.applyParam(m, str, result);
    });
    return str;
  },

  applyOptional: function(p, str, result) {
    var self = this;
    var clone = str.slice(0);
    var resultClone = $.extend({}, result);
    try {
      p.matches.forEach(function(m) {
        str = self.applyParam(m, str, result);
      });
    } catch (ex) {
      // ignore it for optional
      Object.keys(result).forEach(function(key) {
        delete result[key];
      });
      Object.keys(resultClone).forEach(function(key) {
        result[key] = resultClone[key];
      });
      return clone;
    }
    return str;
  },

  applySeparator: function(p, str, result) {
    return this.applyMatch(p, str, result);
  }
};

var escapeStringForRegExp = function(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
};


var RAWMATCH = function(match, flags) {
  return function(fieldName) {
    return {
      kind: "match",
      match: match,
      flags: flags,
      fieldName: fieldName
    };
  };
};

var MATCH = function(match, flags) {
  return RAWMATCH("(" + match + ")", flags);
};

var OPTIONAL = function() {
  return {
    matches: [].slice.call(arguments),
    kind: "optional"
  };
};

var MANDATORY = function() {
  var separator = " ";
  var args = [].slice.call(arguments);
  if (typeof args[0] === "string") {
    separator = args.shift();
  }

  return {
    matches: args,
    kind: "mandatory",
    separator: separator
  };
};

var SEPARATOR = function(separator) {
  var m;
  var byDefault = typeof separator === 'undefined';
  if (byDefault) {
    m = "\\s+";
  } else {
    m = escapeStringForRegExp(separator);
  }
  return {
    match: m,
    default: byDefault,
    kind: "separator"
  };
};


// ======================================== SCHEMA
var schema = [
	MANDATORY(
  	MATCH("CMD")("command")
  ),
  OPTIONAL(
  	SEPARATOR(),
  	MATCH("NEXT")("direction")
  )
];

// ======================================== TESTS
var parse = function(str) {
	var parser = new Parser();
  try {
    return parser.parse(str, schema);
  }
  catch (ex) {
    return false;
  }
}

var expect = function(result, expected, message) {
	var log = $("#log");
  var valid = JSON.stringify(result) === JSON.stringify(expected);
  var h = "<li";
  if (valid) {
  	h += ' class="green"';
  }
  h += ">" + message;
  if (!valid) {
  	h += " expected " + JSON.stringify(result);
    h += " to be: " + JSON.stringify(expected);
  }
  h += "</li>"
  log.append(h);
}

expect(parse("CMD"), { command: "CMD" }, "CMD");
expect(parse("CMD invalid"), false, "CMD with invalid");
expect(parse("CMD NEXT"), { command: "CMD", direction: "NEXT" }, "CMD with NEXT");
expect(parse("CMD NEXT invalid"), false, "CMD NEXT with invalid");
expect(parse("NEXT"), false, "CMD missed");
